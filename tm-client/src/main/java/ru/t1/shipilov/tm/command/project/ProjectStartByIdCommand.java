package ru.t1.shipilov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.dto.request.ProjectStartByIdRequest;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-start-by-id";

    @NotNull
    private final String DESCRIPTION = "Start project by id.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().startProjectById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
