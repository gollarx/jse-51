echo off
xcopy ..\.git .\.git /i /s /e /y
docker build -t tm-logger:latest .
rmdir /s /q .git
pause