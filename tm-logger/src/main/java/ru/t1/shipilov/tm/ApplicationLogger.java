package ru.t1.shipilov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.component.Bootstrap;

public final class ApplicationLogger {

    public static void main(@Nullable String... args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
