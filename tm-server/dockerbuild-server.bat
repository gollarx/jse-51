echo off
xcopy ..\.git .\.git /i /s /e /y
xcopy ..\tm-domain .\tm-domain /i /s /e /y
docker build -t tm-server:latest .
rmdir /s /q .git
rmdir /s /q tm-domain
pause