package ru.t1.shipilov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shipilov.tm.api.service.IPropertyService;
import ru.t1.shipilov.tm.api.service.IServiceLocator;
import ru.t1.shipilov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.shipilov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.shipilov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.shipilov.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.shipilov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setAuthorEmail(propertyService.getAuthorEmail());
        response.setAuthorName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
